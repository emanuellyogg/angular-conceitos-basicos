import { Containership, Spacecraft } from "./base-ship";
import { MillenniumFalcon } from "./starfighter";

import * as _ from 'lodash'
//usamos o autocomplete do lodash....
console.log(_.pad("Typescript Examples", 40, "="));

let ship = new Spacecraft('hyperdrive');
//acessando o método da Classe:
ship.jumpIntoHyperspace();

let falcon = new MillenniumFalcon();
falcon.jumpIntoHyperspace();

// função com parametro do tipo Objeto
let goodForTheJob = (ship: Containership) => ship.cargoContainers > 2;
console.log(`Is falcon good for the job? ${goodForTheJob(falcon) ? 'Yes' : 'no'}`);
