import {Spacecraft, Containership} from './base-ship'

export class MillenniumFalcon extends Spacecraft implements Containership {

  // atributo obrigatório devido a Interface
  cargoContainers: number;

  constructor() {
    //usando a palavra 'super' para usar diretamente da classe que está herdando...
    super('hyperdrive');
    this.cargoContainers = 5;
  }

  // é possível sobrescrever o método da classe base ao mesmo tempo usar o super...
  jumpIntoHyperspace() {
    if (Math.random() >= 0.5) {
      super.jumpIntoHyperspace();
    } else {
      console.log(`Failed to jump into hyperspace`);
    }
  }
}