"use strict";
exports.__esModule = true;
var base_ship_1 = require("./base-ship");
var starfighter_1 = require("./starfighter");
var _ = require("lodash");
//usamos o autocomplete do lodash....
console.log(_.pad("Typescript Examples", 40, "="));
var ship = new base_ship_1.Spacecraft('hyperdrive');
//acessando o método da Classe:
ship.jumpIntoHyperspace();
var falcon = new starfighter_1.MillenniumFalcon();
falcon.jumpIntoHyperspace();
// função com parametro do tipo Objeto
var goodForTheJob = function (ship) { return ship.cargoContainers > 2; };
console.log("Is falcon good for the job? " + (goodForTheJob(falcon) ? 'Yes' : 'no'));
