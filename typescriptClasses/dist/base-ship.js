"use strict";
exports.__esModule = true;
exports.Spacecraft = void 0;
var Spacecraft = /** @class */ (function () {
    //aqui é definido o que é necessário para 'construir' um novo objeto 
    function Spacecraft(propulsor) {
        this.propulsor = propulsor;
    }
    //método (função) não precisa iniciar com a palavra reservada function 
    Spacecraft.prototype.jumpIntoHyperspace = function () {
        console.log("Entering hyperspace with " + this.propulsor);
    };
    return Spacecraft;
}());
exports.Spacecraft = Spacecraft;
