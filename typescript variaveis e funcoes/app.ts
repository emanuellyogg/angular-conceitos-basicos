let message: string = "Help me, Obi-Wan Kenobi"

console.log(message);

let episode: number = 4;

console.log("This is episode " + episode);

episode = episode + 1;

console.log("Next episode is " + episode);

let favoriteDroid: string;

favoriteDroid = "BB-8";

console.log("My favorite droid is " + favoriteDroid);

// ------------------------------------------------------------ EXEMPLO DE FUNÇÕES

// apenas declarando uma função sem retorno;
let call: (name: string) => void;
call = name => console.log("Do you copy, " + name + "?");
call("R2");


// função do tipo arrow function. Arrow function não precisa declarar retorno.
// let ties = ships.filter(ship => ship.type === 'TieFighter');

// função passando dois parametros obrigatórios:
function inc(speed: number, inc: number): number {
  return speed + inc;
}
inc(5, 1);

// função em que um dos parametros não é obrigatório informar:
function inc02(speed: number, inc02?: number): number {
  return speed + inc02;
}
inc02(5, 1);
inc02(5);

// função em que é declarado o valor do paramentro dentro da função:
function inc03(speed: number, inc03: number = 1): number {
  return speed + inc03;
}
inc03(5, 1);
inc03(5);

// função com parametros REST, a chamada dessa função é mais simples:
function countJedis(...jedis: number[]): number {
  return jedis.reduce((a, b) => a + b, 0);
}
countJedis(2, 3, 4);

// função
let isEnoughToBeatMF = function (parsecs: number): boolean {
  return parsecs < 12;
}

let distance = 11;
// exemplo de IF ternário:
console.log(`Is ${distance} parsecs enough to beat Millennium Falcon? ${isEnoughToBeatMF(distance) ? 'YES' : 'NO'}`);