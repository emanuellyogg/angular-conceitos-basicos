class Spacecraft {

  //aqui é definido o que é necessário para 'construir' um novo objeto 
  constructor(public propulsor: string) { }

  //método (função) não precisa iniciar com a palavra reservada function 
  jumpIntoHyperspace() {
    console.log(`Entering hyperspace with ${this.propulsor}`);
  }
}


// INTERFACE
interface Containership {

  cargoContainers: number;
}

//criando o MÓDULO quando são mais de um item para exportar...
export {Spacecraft, Containership}