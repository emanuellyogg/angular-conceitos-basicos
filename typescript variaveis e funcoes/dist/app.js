var message = "Help me, Obi-Wan Kenobi";
console.log(message);
var episode = 4;
console.log("This is episode " + episode);
episode = episode + 1;
console.log("Next episode is " + episode);
var favoriteDroid;
favoriteDroid = "BB-8";
console.log("My favorite droid is " + favoriteDroid);
// ------------------------------------------------------------ EXEMPLO DE FUNÇÕES
// apenas declarando uma função sem retorno;
var call;
call = function (name) { return console.log("Do you copy, " + name + "?"); };
call("R2");
// função do tipo arrow function. Arrow function não precisa declarar retorno.
// let ties = ships.filter(ship => ship.type === 'TieFighter');
// função passando dois parametros obrigatórios:
function inc(speed, inc) {
    return speed + inc;
}
inc(5, 1);
// função em que um dos parametros não é obrigatório informar:
function inc02(speed, inc02) {
    return speed + inc02;
}
inc02(5, 1);
inc02(5);
// função em que é declarado o valor do paramentro dentro da função:
function inc03(speed, inc03) {
    if (inc03 === void 0) { inc03 = 1; }
    return speed + inc03;
}
inc03(5, 1);
inc03(5);
// função com parametros REST, a chamada dessa função é mais simples:
function countJedis() {
    var jedis = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        jedis[_i] = arguments[_i];
    }
    return jedis.reduce(function (a, b) { return a + b; }, 0);
}
countJedis(2, 3, 4);
// função
var isEnoughToBeatMF = function (parsecs) {
    return parsecs < 12;
};
var distance = 11;
// exemplo de IF ternário:
console.log("Is " + distance + " parsecs enough to beat Millennium Falcon? " + (isEnoughToBeatMF(distance) ? 'YES' : 'NO'));
